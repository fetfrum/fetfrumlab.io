#!/bin/bash
set -o verbose 

USER=51914980
COURSE=1037358252
TYPE=orgs #  user или orgs
GIT=goit-kruglenko

header(){
    HEADER=$(echo Authorization: OAuth realm=\"Schoology API\", oauth_consumer_key=\"${CONSUMER_KEY}\",    oauth_token=\"\",    oauth_nonce=\"$( date +%s%N | md5sum | cut -d ' ' -f 1 )\",    oauth_timestamp=\"$( date +%s )\",    oauth_signature_method=\"PLAINTEXT\",    oauth_version=\"1.0\",    oauth_signature=\"${CONSUMER_SECRET}%26\")
}


function rest-api(){

    local ADDRESS
    local HEADER
    local TOTAL
    local LIST
    local ITEM

    case $1 in

        grade_items )
                    header
                    TOTAL=$(curl -o- -s -H "$(echo $HEADER)" http://api.schoology.com/v1/sections/$COURSE/grade_items | jq ".total" ) 
                    header
                    RESULT=$(curl -o- -s -H "$(echo $HEADER)" https://api.schoology.com/v1/sections/$COURSE/grade_items?start=0\&limit=$TOTAL)
                    ;;
        user    )   header
                    RESULT=$(curl -o- -s -H "$(echo $HEADER)" http://api.schoology.com/v1/users/$USER ) 
                    ;;  
                
        get_items ) 
                    header
                    TOTAL=$(curl -o- -s -H "$(echo $HEADER)" http://api.schoology.com/v1/sections/$COURSE/grade_items | jq ".total" ) 
                    header
                    LIST=$(curl -o- -s -H "$(echo $HEADER)" https://api.schoology.com/v1/sections/$COURSE/grade_items?start=0\&limit=$TOTAL | jq '.assignment[].grade_item_id' )
                    declare -a array
                    array=(`echo $LIST | tr '\n' ' '`)
                    echo >  _data/items.json 

                    RESULT="[{"items":[]}]"                                
                    for ITEM in "${array[@]}"; do
                    if [ "$ITEM" -ne "0" ]; then
                    header
                    CURL=$(curl -o- -s -H "$HEADER" https://api.schoology.com/v1/sections/$COURSE/submissions/$ITEM | jq '.[] | select(.revision.revision_id | length >= 1)')
                    echo $CURL

                    # JSON=$( echo  $CURL | jq 'max_by(revision_id)') 
                    
                    # MAX=$(echo $JSON | jq --arg v $ITEM '. += {   "id": $v }')
                    # echo $MAX

                    # RESULT=$(echo $RESULT | $JSON
                    fi
                    done
                          
                    ;;
        assignments ) 
                    header
                    TOTAL=$(curl -o- -s -H "$(echo $HEADER)" http://api.schoology.com/v1/sections/$COURSE/assignments | jq ".total" ) 

                    header
                    RESULT=$(curl -o- -s -H "$(echo $HEADER)" https://api.schoology.com/v1/sections/$COURSE/assignments?start=0\&limit=$TOTAL)
                    ;;

        events  )   header
                    TOTAL=$(curl -o- -s -H "$(echo $HEADER)" http://api.schoology.com/v1/users/$USER/events | jq ".total" ) 

                    header
                    RESULT=$(curl -o- -s -H "$(echo $HEADER)" http://api.schoology.com/v1/users/$USER/events?start=0\&limit=$TOTAL)
                    ;; 
        grades  )   header
                    TOTAL=$(curl -o- -s -H "$(echo $HEADER)" http://api.schoology.com/v1/users/$USER/grades | jq ".total" ) 

                    header
                    RESULT=$(curl -o- -s -H "$(echo $HEADER)" http://api.schoology.com/v1/users/$USER/grades?start=0\&limit=$TOTAL)
                    ;;

        github  )   RESULT=$(curl -o- -s https://api.github.com/$TYPE/$GIT/repos ) 
                    ;;
    esac

    return 0
}



function get-repos-files(){
    contents_url=$(curl -u $GIT_USERNAME:$GIT_TOKEN -o- -s https://api.github.com/$TYPE/$GIT/repos  | jq '.[].contents_url ' | sort | sed 's/\"//g; s/{+path}//')
    for element in "${contents_url[@]}"
    do
        files=$(curl -u $GIT_USERNAME:$GIT_TOKEN -o- -s $element | jq '.[] | select(.type=="file") | select(.name!="README.md" ) | {git_link: .html_url}' | jq '.git_link' | sed 's/\"//g')
        directories=$(curl -u $GIT_USERNAME:$GIT_TOKEN -o- -s $element | jq '.[] | select(.type=="dir")'| jq '.html_url' | sed 's/\"//g; s/$/\/index.html/' )
    done

    all=$(echo "${files[@]}" | grep html; echo "${directories[@]}")
    RESULT=$(echo "$all" | awk -F "/" '{print "-\n  dz: " $5 "\n  url: " $1 "//" $4 ".github.io/" $5 "/" $8 "\n  filename: " $8 }')
#    echo "url:" > _data/files.yml
    echo "$RESULT" > _data/files.yml
}





#rest-api get_items  
#echo $RESULT >_data/items.json
rest-api grades
echo $RESULT | jq . > _data/grades.json
rest-api assignments
echo $RESULT | jq . > _data/assignments.json
rest-api grade_items
echo $RESULT | jq . > _data/grade_items.json
rest-api events
echo $RESULT | jq . > _data/events.json
rest-api user
echo $RESULT | jq . > _data/user.json    
rest-api github
echo $RESULT | jq . > _data/repos.json

echo "current: $(date +%s)" >_data/unixtime.yml 
get-repos-files
echo "git: $GIT" >>_data/unixtime.yml
echo "type: $TYPE" >>_data/unixtime.yml
