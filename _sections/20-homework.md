---
title: Домашние работы
icon: gears
description: Orange juice is juice from oranges. It's made by squeezing oranges.
---
<div class="container">
<div class="row">  
<h2>Домашние работы по курсу GoFrontend2</h2>

<table class="table table-striped table-hover table-condensed">
    <tr>
        <th>#</th>
        <!-- <th>Тема</th> -->
        <th>Срок сдачи работы</th>
        <th>Сколько дней просрочено</th>
        <!-- <th>Репозиторий</th> -->
        <th>Заработал баллов</th>
        <th>Репозиторий, ссылки на задания</th>
    </tr>
    
    {% assign grading_category = site.data.grades.section.first.grading_category %}
    {% assign user_grades = site.data.grades.section.first.period.first.assignment %}
    {% assign repos = site.data.repos %}
    {% assign items = site.data.items %}
    {{ site.data.items }}
    <!-- Тут запускаем цикл обработки REST-API-списка с работами -->
    {% for assignment in site.data.assignments.assignment offset:1 %}
    {% assign current_grade = user_grades | where: "assignment_id", assignment.id %}
    {% assign due-date = assignment.due | date: '%s' %}
    {% assign lag = site.data.unixtime.current | minus: due-date  %}
    {% assign grade_after_lag = due-date | minus:   current_grade.first.timestamp  %}
    {% assign items = site.data.items %}

    <tr><!-- <td>
        {% for grade_items in site.data.grade_items.assignment %}
            {% if grade_items.id ==  assignment.id  %}
                {% assign item=grade_items %}
                {{ item.grade_item_id  }}   
            {% endif %}
        {% endfor %}
        </td> -->
        <!-- Ссылка на задание в Скулоджи -->
        <td><a href="https://app.schoology.com/assignment/{{ assignment.id }}/info">
         {% assign assignment_num = assignment.title | remove: "Домашнее задание" | remove: "Homework_" | remove: ")" | replace: "(", "." | split: '.' %}
         {{ assignment_num[0] | to_integer }}{% if assignment_num[1] != null %}.<small>{{assignment_num[1]}}</small>
        {% endif %}</a>
        </td>
        <!-- Название общей темы -->
        <!-- <td><a href="https://app.schoology.com/assignment/{{ assignment.id }}/info">
        {% assign category_id = assignment.grading_category | where: "id", assignment.grading_category %} 
        {{ category_id.first.title }}</a>
        </td> -->
        <td>
        <!-- Срок сдачи работы -->
        {{ assignment.due |  date: ' %d %B %Y'}}
        </td>
        <!--    <td>
            Дата сдачи работы
        {$ assign grade_day | where: items,  $}
        </td> -->
        <td>
        <!-- Просрочено дней -->
        {% if current_grade.first.timestamp == 0 %}
            {% if lag > 0%}
                {{ lag | divided_by: 60 | divided_by: 60 | divided_by: 24}}
            {% endif %}
        {% else %}
        {% endif %}
        </td>
       <!--      <td>
            {% if current_grade.first.timestamp > 0 %}
            {{ grade_after_lag | divided_by: 60 | divided_by: 60 | divided_by: 24 }}
            {% endif%}
        </td> -->
        <td>
        <!-- Заработал баллов -->
        {{ current_grade.first.grade }}
        </td>
        <td>
          {% if assignment_num[1] != null %}
          {% assign number =  assignment_num[0] | to_integer | times: 10 | plus: assignment_num[1] | times: 0.1  %}
          {% else %}
          {% assign number =  assignment_num[0] | to_integer %}
          {% endif %}
          {% assign repo_name = "DZ_"  | append:  number %}
          {% for repo in repos %}
          {% if repo_name == repo.name %}
                <a href="#" data-toggle="modal" class="large" data-target="#{{ repo.name | replace: '.', '_'}}Modal">
                    <i class="fa fa-github "></i> Ссылки на GitHub и на GitPages 
                </a>
          {% endif %}
          {% endfor %} 
        </td>
    </tr>{% endfor  %}

</table>
{% for assignment in site.data.assignments.assignment offset:1 %}
    {% assign assignment_num = assignment.title | remove: "Домашнее задание" | remove: "Homework_" | remove: ")" | replace: "(", "." | split: '.' %}
    {% if assignment_num[1] != null %}
        {% assign number =  assignment_num[0] | to_integer | times: 10 | plus: assignment_num[1] | times: 0.1  %}
    {% else %}
        {% assign number =  assignment_num[0] | to_integer %}
    {% endif %}
    {% assign repo_name = "DZ_"  | append:  number %}
    {% for repo in repos %}
        {% if repo_name == repo.name %}
           <div class="modal fade" id="{{ repo.name | replace: '.', '_'}}Modal" tabindex="-1" role="dialog" aria-labelledby="{{ repo.name | replace: '.', '_'}}ModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="{{ repo.name | replace: '.', '_'}}ModalLabel">Задание {{ number }} <a href="https://github.com/{{unixtime.git}}/{{repo.name}}/"></a><i class="fa fa-github "></i></h4>
                    
                  </div>
                  <div class="modal-body">
                  {% for item in site.data.files %}
                    {% if item.dz==repo.name %} 
                         <a href="{{ item.url }}">Упражнение {{item.filename}}</a><br>
                    {% endif %}
                  {% endfor %}
                   
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                  </div>
                </div>
              </div>
            </div>
        {% endif %}
    {% endfor %} 
{% endfor %} 

</div>
</div>