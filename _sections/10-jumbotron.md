---
title: Главная
icon: home
---
<div class="container">
<div class="row">
    <div class="col-xs-12 col-md-6 col-sm-6"></div>
        <div class="col-xs-12 col-md-6 col-sm-6 ">
            <div class="panel panel-info">                 
	            <div class="panel-heading">
	                <h1 class="panel-title">Немного личного пространства</h1>
	            </div>             
	            <div class="panel-body">                     
	                <p>На этом сайте я планирую размещать ссылки на домашние работы по курсу GoFrontend#2, фиксировать собственную успеваемость, а также публиковать ссылки на публичные репозитории с домашними заданиями.</p>
	                <p>Также в планах неспешное ведение блога на тему IT-технологий, не только по фронтэнду, но по всем актуальным для меня технологиям: Linux, Docker, Rancher, FreeRTOS, Blockchain и многим другим...</p>       
	            </div>
	        </div>
        </div>
    </div>
</div>


        
     