# -*- encoding: utf-8 -*-
# stub: jekyll-less 0.0.4 ruby lib

Gem::Specification.new do |s|
  s.name = "jekyll-less".freeze
  s.version = "0.0.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Roger L\u{f3}pez".freeze]
  s.date = "2011-12-04"
  s.description = "Convert Less CSS files to standard CSS files as part of your Jekyll build.".freeze
  s.email = ["roger@zroger.com".freeze]
  s.homepage = "http://github.com/zroger/jekyll-less".freeze
  s.rubyforge_project = "jekyll-less".freeze
  s.rubygems_version = "2.5.2".freeze
  s.summary = "Less CSS converter for Jekyll".freeze

  s.installed_by_version = "2.5.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<jekyll>.freeze, [">= 0.10.0"])
      s.add_runtime_dependency(%q<less>.freeze, [">= 2.0.5"])
    else
      s.add_dependency(%q<jekyll>.freeze, [">= 0.10.0"])
      s.add_dependency(%q<less>.freeze, [">= 2.0.5"])
    end
  else
    s.add_dependency(%q<jekyll>.freeze, [">= 0.10.0"])
    s.add_dependency(%q<less>.freeze, [">= 2.0.5"])
  end
end
