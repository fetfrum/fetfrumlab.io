# -*- encoding: utf-8 -*-
# stub: jekyll-asset-pipeline 0.1.2 ruby lib

Gem::Specification.new do |s|
  s.name = "jekyll-asset-pipeline".freeze
  s.version = "0.1.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Matt Hodan".freeze]
  s.date = "2012-12-26"
  s.description = "Adds asset preprocessing (CoffeeScript, Sass, Less, ERB, etc.) and asset compression/minification/gzip (Yahoo YUI Compressor, Google Closure Compiler, etc.) to Jekyll.".freeze
  s.email = "matthew.c.hodan@gmail.com".freeze
  s.homepage = "http://www.matthodan.com/2012/11/22/jekyll-asset-pipeline.html".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "2.5.2".freeze
  s.summary = "A powerful asset pipeline for Jekyll that bundles, converts, and minifies CSS and JavaScript assets.".freeze

  s.installed_by_version = "2.5.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<jekyll>.freeze, [">= 0.10.0"])
      s.add_runtime_dependency(%q<liquid>.freeze, [">= 1.9.0"])
      s.add_development_dependency(%q<rake>.freeze, [">= 0.9"])
      s.add_development_dependency(%q<minitest>.freeze, [">= 4.3"])
    else
      s.add_dependency(%q<jekyll>.freeze, [">= 0.10.0"])
      s.add_dependency(%q<liquid>.freeze, [">= 1.9.0"])
      s.add_dependency(%q<rake>.freeze, [">= 0.9"])
      s.add_dependency(%q<minitest>.freeze, [">= 4.3"])
    end
  else
    s.add_dependency(%q<jekyll>.freeze, [">= 0.10.0"])
    s.add_dependency(%q<liquid>.freeze, [">= 1.9.0"])
    s.add_dependency(%q<rake>.freeze, [">= 0.9"])
    s.add_dependency(%q<minitest>.freeze, [">= 4.3"])
  end
end
