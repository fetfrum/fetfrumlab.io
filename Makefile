all: 
	rm -Rf public/
	apt update
	apt install -y jq
	/bin/bash ./curl.sh
	bundle install --path vendor/bundle 
	bundle add jekyll --version '~> 3.4.3' 
	bundle exec jekyll build -d public/

server: 
	cd ~/Work/fetfrum.gitlab.io/
	bundle exec jekyll server -d public/
	rm -f public/server

git:
	git submodule init
	git submodule update

update:
	bundle update

depend:
	sudo apt install ruby2.3
	sudo gem install bundle

bundle:
	bundle install --path vendor/bundle
	bundle add jekyll

check-github:
	git submodule update

clean:
	rm -Rf public




